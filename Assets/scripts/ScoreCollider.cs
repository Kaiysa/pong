﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class ScoreCollider : MonoBehaviour
{

  [SerializeField] private int player1_score = 0;
  [SerializeField] private int player2_score = 0;
  [SerializeField] private Text player1Score;
  [SerializeField] private Text player2Score;
  private Rigidbody2D ballrb;
  private float rand;


    void FixedUpdate()
    {
      rand = Random.Range(0,2);
      if((player1_score == 5)|| (player2_score == 5))
      {
        //freeze all positions and wait for Input.anyKey.
        //restart function! external method???
        SceneManager.LoadScene("Gameplay_Scene");
      }
    }
    void OnCollisionEnter2D(Collision2D goalCollision)
    {

      //goalCollision.gameObject - wall
      //goalCollision.transform.position - wall.ballPosition
      //goalCollision.collider - wall.collider

      if(goalCollision.gameObject.name == "left_wall")
      {
        player2_score++;
        player2Score.text = player2_score.ToString();
        resetPosition();
      }
      if(goalCollision.gameObject.name == "right_wall")
      {
        player1_score++;
        player1Score.text = player1_score.ToString();
        resetPosition();
      }

    }

    void resetPosition()
    {
      transform.position = new Vector2(0, 0);
      float startSpeed = 3;
       if(rand < 1)
       {
         GetComponent<Rigidbody2D>().velocity = new Vector2(-1,0) * startSpeed;
       }
       else
       {
         GetComponent<Rigidbody2D>().velocity = new Vector2(1,0) * startSpeed;
       }
    }





}
