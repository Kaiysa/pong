﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ball_Movement : MonoBehaviour
{
    [SerializeField] private float speed = 3;
    [SerializeField] private GameManager gm;

    void Start()
    {
    	GetComponent<Rigidbody2D>().velocity = Vector2.right * 3;

    }

    void OnCollisionEnter2D(Collision2D ballColision)
    {
      //ballColision.gameObject - platform
      //ballColision.transform.position - platform.position
      //ballColision.collider - platform.collider

      if(ballColision.gameObject.name == "player1")
      {
        //ball hit
        float y = ballHit(transform.position,
                            ballColision.transform.position,
                            ballColision.collider.bounds.size.y);

        //direction of ball
        Vector2 direction = new Vector2(1,y).normalized;
        GetComponent<Rigidbody2D>().velocity = direction * speed;
      }

      if(ballColision.gameObject.name == "player2")
      {
        //changing hit angle
        float y = ballHit(transform.position,
                          ballColision.transform.position,
                          ballColision.collider.bounds.size.y);

        Vector2 direction = new Vector2(-1,y).normalized;
        GetComponent<Rigidbody2D>().velocity = direction * speed;

      }
    }

    float ballHit(Vector2 ballPosition, Vector2 platformPosition, float platformHight)
    {

      return (ballPosition.y - platformPosition.y) / platformHight;
    }



}
